
#pragma once

#ifndef GREET_H
#define GREET_H

void greet(const char * const whom);

#endif /* GREET_H */
