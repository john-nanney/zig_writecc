
#include <stdio.h>

#include "greet.h"

int main(void)
{
    greet("Billy");
    greet(0);

    return 0;
}
