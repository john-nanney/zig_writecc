
#include <stdio.h>

#include "greet.h"

void greet(const char * const whom)
{
    printf("Hello %s!\n",
            whom ? whom : "world"
          );
}

