const std = @import("std");
const ArrayList = std.ArrayList;

pub fn maybeCreateCompileCommands(_: *const std.build.Builder.Step, _: *std.Progress.Node) !void {
    const dir = try std.fs.cwd().openIterableDir("compile_commands", .{});
    var iterator = dir.iterate();

    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();

    defer {
        if (.ok != gpa.deinit()) {
            @panic("OFSMC!  Some memory leaked!");
        }
    }

    var my_file = std.fs.cwd().createFile("compile_commands.json", .{}) catch |err| {
        std.debug.print("\nError: {}\n", .{err});
        return;
    };
    defer my_file.close();

    var comma: bool = false;

    var wrote_bytes = my_file.write("[\n") catch |err| {
        std.debug.print("\nError writing: {}\n", .{err});
        return err;
    };

    while (try iterator.next()) |path| {
        var relative_name = std.ArrayList(u8).init(allocator);
        defer relative_name.deinit();

        relative_name.appendSlice("compile_commands/") catch |err| {
            std.debug.print("\nError building string: {}\n", .{err});
            return err;
        };

        relative_name.appendSlice(path.name) catch |err| {
            std.debug.print("\nError building string: {}\n", .{err});
            return err;
        };

        var json_file = try std.fs.cwd().openFile(relative_name.items, .{});
        defer json_file.close();

        var cc_json_string: [1024 * 1024]u8 = undefined;
        const bytes_read = try json_file.read(&cc_json_string);

        var last_byte: usize = bytes_read;
        if (cc_json_string[(bytes_read - 2)] == ',') {
            last_byte -= 2;
        }

        const stream_bytes = cc_json_string[0..last_byte];

        if (comma) {
            wrote_bytes = my_file.write(",") catch |err| {
                std.debug.print("\nError writing: {}\n", .{err});
                return err;
            };
        }

        wrote_bytes = my_file.write(stream_bytes) catch |err| {
            std.debug.print("\nError writing: {}\n", .{err});
            return err;
        };

        comma = true;
    }

    wrote_bytes = my_file.write("\n]\n") catch |err| {
        std.debug.print("\nError: {}\n", .{err});
        return err;
    };
}

pub fn build(b: *std.build.Builder) void {
    const target = b.standardTargetOptions(.{});

    const cflags = .{
        "-gen-cdb-fragment-path",
        "compile_commands",
        "-Wall",
        "-W",
        "-g",
        "-O2",
        "-fstack-protector-strong",
        "-D_FORTIFY_SOURCE=2",
        "-Wstrict-prototypes",
        "-Wwrite-strings",
        "-Wno-missing-field-initializers",
        "-fno-omit-frame-pointer",
    };

    const greet_options = std.build.Builder.StaticLibraryOptions{
        .name = "greet",
        .optimize = std.builtin.Mode.Debug,
        .target = target,
    };

    const greet = b.addStaticLibrary(greet_options);
    greet.linkLibC();
    greet.force_pic = true;
    greet.addCSourceFiles(&.{"src/greet.c"}, &cflags);

    const hello_options = std.build.Builder.ExecutableOptions{
        .name = "greet",
        .optimize = std.builtin.Mode.Debug,
        .target = target,
    };

    const hello = b.addExecutable(hello_options);
    hello.linkLibrary(greet);
    hello.addLibraryPath("/usr/local/lib");
    hello.linkLibC();
    hello.addCSourceFiles(&.{"src/hello.c"}, &cflags);
    b.installArtifact(hello);

    const run_cmd = b.addRunArtifact(hello);
    // const run_cmd = hello.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const write_compile_commands_step = b.step("writecc", "Write compile_commands.json");
    write_compile_commands_step.makeFn = maybeCreateCompileCommands;
    write_compile_commands_step.dependOn(&hello.step);
}
