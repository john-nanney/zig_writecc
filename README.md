
# Build Compile Commands with Zig

This is an example build file that uses clang's `-gen-cdb-fragment-path` to
write a compile commands fragment, and then gathers them all together into a
single `compile_commands.json` file.

To build the project and create `copmile_commands.json` run:

    $ zig build writecc

## Notes

This project is really just my way of remembering how to build a C/C++ project
with Zig.  My editor uses clangd when editing C/C++ code, and after it
complained one too many times about missing `compile_commands.json` I decided
to add that ability to my build file.

Requires a recent version of Zig, probably recent enough to run ziglings.
